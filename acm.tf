resource "aws_acm_certificate" "ssl_certificate" {
  domain_name       = "dev.berkeley.huantech.net"
  validation_method = "DNS"

  tags = {
    Name = "SSL Certificate"
  }
}

locals {
  validation_options_map = { for idx, val in tolist(toset(aws_acm_certificate.ssl_certificate.domain_validation_options)) : idx => val }
}

resource "aws_route53_record" "ssl_validation" {
  for_each = local.validation_options_map

  zone_id = "Z0106045BMT2DFABHWH1"
  name    = each.value.resource_record_name
  type    = "CNAME"
  ttl     = "300"

  records = [each.value.resource_record_value]

  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "ssl_validation" {
  certificate_arn         = aws_acm_certificate.ssl_certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.ssl_validation : record.fqdn]
}

