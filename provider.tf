terraform {
  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = "2.4.0"  # Replace with the desired version number for archive provider
    }
    aws = {
      source  = "hashicorp/aws"
      version = "5.26.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"  # Replace with the desired version number for random provider
    }
  }
}
