terraform {
  backend "s3" {
        bucket = "govtech-cds-assessment-tf-statefile"
        key    = "berkeley-iac-dev"
        region = "ap-southeast-1"
  }
}
provider "aws" {
  region = "ap-southeast-1"  # Replace with your desired region
}

# Create the EKS cluster
resource "aws_eks_cluster" "berkeley-eks-app" {
  name     = "berkeley-eks-app-${var.environment}"
  role_arn = aws_iam_role.berkeley-eks-cluster-role.arn
  version  = "1.28"  # Replace with your desired EKS version

  vpc_config {
    subnet_ids         = ["subnet-dd599e95", "subnet-1ca4727a", "subnet-dfdd5086"]  # Replace with your desired subnet IDs
    security_group_ids = ["sg-02a15076d78d7ea0f"]  # Replace with your desired security group IDs
  }

}
resource "aws_eks_addon" "berkeley-eks-vpc-cni" {
  cluster_name                = "berkeley-eks-app-${var.environment}"
  addon_name                  = "vpc-cni"
  addon_version               = "v1.14.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"

}

resource "aws_eks_addon" "berkeley-eks-core-dns" {
  cluster_name                = "berkeley-eks-app-${var.environment}"
  addon_name                  = "coredns"
  addon_version               = "v1.10.1-eksbuild.2"
  resolve_conflicts_on_create = "OVERWRITE"

}

resource "aws_eks_addon" "berkeley-eks-kube-proxy" {
  cluster_name                = "berkeley-eks-app-${var.environment}"
  addon_name                  = "kube-proxy"
  addon_version               = "v1.28.1-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"

}
resource "aws_eks_addon" "berkeley-eks-eks-pod-identity-agent" {
  cluster_name                = "berkeley-eks-app-${var.environment}"
  addon_name                  = "eks-pod-identity-agent"
  addon_version               = "v1.0.0-eksbuild.1"
  resolve_conflicts_on_create = "OVERWRITE"

}



resource "aws_eks_node_group" "berkeley-eks-app-node-group" {
  cluster_name    = aws_eks_cluster.berkeley-eks-app.name
  node_group_name = "berkeley-eks-app-${var.environment}-node-group"
  node_role_arn   = aws_iam_role.berkeley-eks-node-group-role.arn
  subnet_ids      = ["subnet-dd599e95", "subnet-1ca4727a", "subnet-dfdd5086"]  # Replace with your desired public subnet IDs
  capacity_type   = "SPOT"
  instance_types  = ["t2.small"]  # Specify the desired instance types here

  scaling_config {
    desired_size = 1  # Replace with your desired capacity
    min_size     = 1  # Replace with your desired minimum size
    max_size     = 5  # Replace with your desired maximum size
  }

  # Other optional configurations
  disk_size = 20  # Replace with your desired disk size in GB
}

# Create the IAM role for the EKS cluster
resource "aws_iam_role" "berkeley-eks-cluster-role" {
  name = "berkeley-eks-cluster-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service":  ["eks.amazonaws.com"]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role" "berkeley-eks-node-group-role" {
  name = "berkeley-eks-node-group-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service":  ["ec2.amazonaws.com"]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "berkeley-eks-node-group-role-attachment-ec2" {
  name       = "berkeley-eks-node-group-role-attachment-ec2"
  roles      = [aws_iam_role.berkeley-eks-node-group-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_policy_attachment" "berkeley-eks-node-group-role-attachment-eks" {
  name       = "berkeley-eks-node-group-role-attachment-eks"
  roles      = [aws_iam_role.berkeley-eks-node-group-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

resource "aws_iam_policy_attachment" "berkeley-eks-node-group-role-attachment-eks-cni" {
  name       = "berkeley-eks-node-group-role-attachment-eks"
  roles      = [aws_iam_role.berkeley-eks-node-group-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}
# Attach the necessary policies to the IAM role
resource "aws_iam_role_policy_attachment" "berkeley-eks-cluster-policy-attachment" {
  role       = aws_iam_role.berkeley-eks-cluster-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_iam_role_policy_attachment" "berkeley-eks-cluster-policy-attachment-eks-cni" {
  role       = aws_iam_role.berkeley-eks-cluster-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}



# Output the EKS cluster details
output "eks_cluster_name" {
  value = aws_eks_cluster.berkeley-eks-app.name
}


