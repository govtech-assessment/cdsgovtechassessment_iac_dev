
variable "environment" {
  description = "Resource Environment"
  type        = string
  default     = "dev"
}